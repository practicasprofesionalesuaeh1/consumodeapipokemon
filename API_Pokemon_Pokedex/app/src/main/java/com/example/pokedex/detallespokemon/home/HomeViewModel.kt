package com.example.pokedex.detallespokemon.home

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pokedex.R
import com.example.pokedex.datospokemon.Menu

class HomeViewModel(private val context: Context) : ViewModel() {

    private val listMenu = MutableLiveData<List<Menu>>()

    fun getListMenu(): LiveData<List<Menu>> {
        listMenu.value = listOf(
            Menu(1, context.resources.getString(R.string.menu_item_1), R.color.lightTeal)
        )
        return listMenu
    }


}
