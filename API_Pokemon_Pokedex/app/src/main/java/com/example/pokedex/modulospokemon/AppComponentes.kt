package com.example.pokedex.modulospokemon

val appComponent = listOf(
    databaseModule,
    networkModule,
    viewModelsModule
)
