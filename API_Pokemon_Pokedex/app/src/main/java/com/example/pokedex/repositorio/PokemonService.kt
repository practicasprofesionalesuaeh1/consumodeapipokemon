package com.example.pokedex.repositorio

import com.example.pokedex.datospokemon.Pokemon
import retrofit2.Call
import retrofit2.http.GET

interface PokemonService {
    @GET("pokemon.json")
    fun get(): Call<List<Pokemon>>
}
