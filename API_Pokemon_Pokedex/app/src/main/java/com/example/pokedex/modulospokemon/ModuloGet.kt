package com.example.pokedex.modulospokemon

import com.example.pokedex.detallespokemon.caracteristicas.DashboardViewModel
import com.example.pokedex.detallespokemon.modeloprueba.InterfaceModelo
import com.example.pokedex.detallespokemon.home.HomeViewModel
import com.example.pokedex.detallespokemon.pokedex.PokedexViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {
    viewModel { DashboardViewModel(get()) }
    viewModel { InterfaceModelo(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { PokedexViewModel(get(), get()) }
}
