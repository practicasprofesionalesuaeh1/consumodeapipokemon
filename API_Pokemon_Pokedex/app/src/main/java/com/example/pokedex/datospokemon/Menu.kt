package com.example.pokedex.datospokemon

data class Menu(
    val id: Int,
    val name: String,
    val color: Int
)
