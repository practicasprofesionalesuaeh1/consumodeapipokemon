package com.example.pokedex.modulospokemon

import androidx.room.Room
import com.example.pokedex.R
import com.example.pokedex.basepokemonapi.AppBD
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidApplication(),
            AppBD::class.java,
            androidApplication().baseContext.getString(R.string.app_name)
        ).build()
    }

    single {
        get<AppBD>().pokemonDAO()
    }
}
