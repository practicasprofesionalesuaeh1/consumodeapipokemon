package com.example.pokedex.detallespokemon.caracteristicas

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.pokedex.basepokemonapi.datospokemon.PokemonDA
import com.example.pokedex.datospokemon.Pokemon

class DashboardViewModel(private val pokemonDAO: PokemonDA) : ViewModel() {

    fun getPokemonById(id: String?): LiveData<Pokemon> {
        return pokemonDAO.getById(id)
    }

    fun getPokemonEvolutionsByIds(ids: List<String>): LiveData<List<Pokemon>> {
        return pokemonDAO.getEvolutionsByIds(ids)
    }
}
