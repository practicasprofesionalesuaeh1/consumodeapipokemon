package com.example.pokedex.basepokemonapi

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.pokedex.basepokemonapi.datospokemon.PokemonDA
import com.example.pokedex.datospokemon.Pokemon

@Database(entities = [Pokemon::class], version = 5, exportSchema = false)
abstract class AppBD : RoomDatabase() {

    abstract fun pokemonDAO(): PokemonDA
}
